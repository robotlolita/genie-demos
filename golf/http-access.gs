/* Access example.com and print the contents to the stdout */
init
	data: string
	
	var url  = "http://www.example.com"
	var f    = File.new_for_uri(url)
	
	try
		f.load_contents(null, out data)
		print data
	except e: Error
		print "Error: %s", e.message
