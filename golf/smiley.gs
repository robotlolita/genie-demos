/* Print an incremental smiley to the stdout for as long as the program runs */
init
	chars: array of string  = {":", "-", ")"}
	
	var smiley = new StringBuilder
	var i      = 0
	
	while true
		smiley.append(chars[i])
		if i < 2 do i++
		print smiley.str
