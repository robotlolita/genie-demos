/* Read the result of sets from the stdin and prints the results of a tennis
 * game to the stdout.
 * 
 * Set results are single digit base 10 integers (either 1 or 2). 1 means that
 * the first player won the set. 2 means the second player won.
 * 
 * When a player is leading the set, the following string is output:
 * 
 *     Player%(pnum)d is leading the set %(score1)d - %(score2)d
 * 
 * 
 * Where `pnum` is the number of the player leading the game, and `score1` and
 * `score2` are respectively the score of first and second players.
 * 
 * When a set is tied (both players have the same score), the following string
 * is output:
 * 
 *     Set is tied at %(score)d
 * 
 * 
 * Where `score` is the score of both players.
 * 
 * When the program finishes reading the stdout, it should announce the result
 * of the game in the following format:
 * 
 *     Player%(pnum)d wins the set %(score1)d - %(score2)d
 */
init
	var p1_score = 0
	var p2_score = 0
	s: string
	
	while (s = stdin.read_line()) is not null
		if s == "1"
			p1_score++
		else if s == "2"
			p2_score++

		if p1_score > p2_score
			print "Player1 is leading the set %d - %d", p1_score, p2_score
		else if p2_score > p1_score
			print "Player2 is leading the set %d - %d", p2_score, p1_score
		else
			print "Set is tied at %d", p1_score


	// set ended, print results
	if p1_score > p2_score
		print "Player1 wins the set %d - %d", p1_score, p2_score
	else if p2_score > p1_score
		print "Player2 wins the set %d - %d", p2_score, p1_score
	else
		print "For some wierd reason the set ended in a tie, which means that" \
			+ " either we don't have enough data, or a comet hit the stadium" \
			+ " before the game ended."
