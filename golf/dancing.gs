/* dancing.gs
 * 
 * Prints cute ASCII art for dancing people according to the input.
 * 
 * Possibly input is: left, right and left&right; referring to which arm is 
 * raised at the given time.
 */

const kid: string = "%s('-')%s \n" \
                  + " %s|_|%s  \n" \
                  +  "  | |      "

def dance(left: bool, right: bool)
	var larm_raise = " "
	var larm_lower = "/"
	var rarm_raise = " "
	var rarm_lower = "\\"
	
	if left
		larm_raise = "\\"
		larm_lower = " "
	if right
		rarm_raise = "/"
		rarm_lower = " "
	
	print kid, larm_raise, rarm_raise, larm_lower, rarm_lower


init
	s: string
	
	while (s = stdin.read_line()) is not null
		case s
			when "left"       do dance(true,  false)
			when "right"      do dance(false, true)
			when "left&right",                                                 \
			     "right&left" do dance(true,  true)
			default           do dance(false, false)
		Thread.usleep((int)5.0e5)
