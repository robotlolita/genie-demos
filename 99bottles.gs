/* 99 Bottles Of Beer implementation in Genie */
init
	for i:int = 99 downto 2
		print "%d bottles of beer on the wall, %d bottles of beer.\n" \
		    + "Take one down and pass it around, %d bottles of beer on " \
			+ "the wall.\n\n", i, i, i - 1

	print "1 bottle of beer on the wall, 1 bottle of beer.\n" \
	    + "Take one down and pass it around, no more bottles of beer on " \
	    + "the wall.\n\n" \
	    + "No more bottles of beer on the wall, no more bottles of beer.\n" \
	    + "Go to the store and buy some more, 99 bottles of beer on the wall."
