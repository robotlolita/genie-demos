/* Counts the number of words and lines in a string.
 * 
 * The input string can be either given as a filename on the first argument, in
 * which case the file is read and its contents analyzed. Or directly in the
 * stdin.
 */
init
	var lines = 0 // number of lines
	var words = 0 // number of words
	var wregx = new Regex("[\\w\\-]+\\s*")   // word matcher regexp
	var input = new StringBuilder
	
	s:string      // string to load file contents
	f:FileStream  // file object
	m:MatchInfo   // match info for word matching
	
	// check if a filename was passed and decide what kind of input to use
	if args[1] is not null
		f = FileStream.open(args[1], "r")
	else
		f = #stdin // transfer the object in stdin to f (stdin is null now :<)
	
	// load input to the string
	while (s = f.read_line()) is not null
		input.append(s)
		lines++
	
	// keep searching the string for words until we run out of them
	if wregx.match(input.str, 0, out m)
		do
			words++
		while m.next()
	
	print "lines: %d\nwords: %d\nchars: %d", lines, words, (int)input.len
